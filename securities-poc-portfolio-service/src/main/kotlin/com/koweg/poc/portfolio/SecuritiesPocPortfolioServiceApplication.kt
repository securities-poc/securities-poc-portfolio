package com.koweg.poc.portfolio

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient
@SpringBootApplication
class SecuritiesPocPortfolioServiceApplication

fun main(args: Array<String>) {
	runApplication<SecuritiesPocPortfolioServiceApplication>(*args)
}
