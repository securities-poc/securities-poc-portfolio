package com.koweg.poc.gw.api.model

import org.springframework.boot.jackson.JsonComponent

@JsonComponent
data class Position (
        var isin: String?,
        var stockName: String?,
        var quantity: String?,
        var watchRate: String?,
        var watchBookCost: String?){
    constructor() : this(null, null,null,null,null)
}

