package com.koweg.poc.gw.api.model

import org.springframework.boot.jackson.JsonComponent
import java.time.LocalDateTime

@JsonComponent
data class Portfolio (
        var date: LocalDateTime?,
        var portfolioName: String?,
        var positions: List<Position>?){
    constructor() : this(null,null,null)
}
